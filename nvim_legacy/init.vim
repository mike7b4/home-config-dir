call plug#begin('~/.local/share/nvim/plugged')
	Plug 'neovim/nvim-lspconfig'
	Plug 'simrat39/rust-tools.nvim'
	Plug 'rust-lang/rust.vim'
	Plug 'lotabout/skim', { 'dir': '~/.skim', 'do': './install' }
	Plug 'lotabout/skim.vim'
	Plug 'scrooloose/nerdtree', {'on' : 'NERDTree' }
	Plug 'morhetz/gruvbox'
	Plug 'dense-analysis/ale'
	Plug 'cespare/vim-toml'
	Plug 'tpope/vim-fugitive'
	Plug 'fedorenchik/qt-support.vim'
call plug#end()

let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 0
set path+=**
set statusline+=%F
set statusline+=%#warningmsg#
set statusline+=%*
set statusline+=(C:\ %c)"
set autochdir
set number
set relativenumber

set spell spelllang=en_us
let g:rustfmt_autosave = 1

let g:ale_rust_rls_config = {
	\   'rust': {
	\     'clippy_preference': 'on'
	\   }
	\ }

let g:ale_rust_cargo_use_check = 1
let g:ale_rust_cargo_check_tests = 1
let g:ale_rust_cargo_use_clippy = 1
let g:ale_sign_column_always = 1
let g:ale_fixers = {
    \ '*': ['remove_trailing_lines', 'trim_whitespace'],
    \ 'rust': ['rustfmt'],
\}


let g:LanguageClient_serverCommands = {
    \ 'rust': ['~/bin/rust-analyzer-linux'],
    \ 'javascript': ['/usr/local/bin/javascript-typescript-stdio'],
    \ 'javascript.jsx': ['tcp://127.0.0.1:2089'],
    \ 'ruby': ['~/.rbenv/shims/solargraph', 'stdio']}

"""let g:ale_rust_rls_executable = '~/bin/rust-analyzer'
set clipboard=unnamedplus
set colorcolumn=80
syntax on
nnoremap <F5> :call LanguageClient_contextMenu()<CR>

nmap <C-p> :SK<CR>
map <C-n> :NERDTree<CR>
map <C-c> :read !head -n4 debian/changelog<CR>

command! -bang -nargs=* Ag call fzf#vim#ag_interactive(<q-args>, fzf#vim#with_preview('right:50%:hidden', 'alt-h'))
command! -bang -nargs=* Rg call fzf#vim#rg_interactive(<q-args>, fzf#vim#with_preview('right:50%:hidden', 'alt-h'))

" Configure LSP through rust-tools.nvim plugin.
" rust-tools will configure and enable certain LSP features for us.
" See https://github.com/simrat39/rust-tools.nvim#configuration

lua <<EOF
local nvim_lsp = require'lspconfig'
require'rust-tools.hover_actions'.hover_actions()

local opts = {
    tools = { -- rust-tools options
        autoSetHints = true,
        hover_with_actions = true,
        inlay_hints = {
            show_parameter_hints = false,
            parameter_hints_prefix = "",
            other_hints_prefix = "",
        },
    },

    -- all the opts to send to nvim-lspconfig
    -- these override the defaults set by rust-tools.nvim
    -- see https://github.com/neovim/nvim-lspconfig/blob/master/CONFIG.md#rust_analyzer
   server = {
        -- on_attach is a callback called when the language server attachs to the buffer
	on_attach = my_attach,
        settings = {
            -- to enable rust-analyzer settings visit:
            -- https://github.com/rust-analyzer/rust-analyzer/blob/master/docs/user/generated_config.adoc
            ["rust-analyzer"] = {
                -- enable clippy on save
                checkOnSave = {
                    command = "clippy"
                },
            }
        }
    },
}

require('rust-tools').setup(opts)
EOF
